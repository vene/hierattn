import dynet as dy
from dyhierattn.utils import BaseSubmodel


class SoftAttention(BaseSubmodel):
    def __init__(self, model, input_dim, hidden_dim):

        self.param_collection_ = model.add_subcollection()
        self.spec = (input_dim, hidden_dim)

        self.input_dim = input_dim
        self.hidden_dim = hidden_dim
        self.W_ = model.add_parameters((hidden_dim, input_dim))
        self.b_ = model.add_parameters(hidden_dim)
        self.context_ = model.add_parameters(hidden_dim)

    def attention(self, x):
        return dy.softmax(x)

    def __call__(self, H):

        W = dy.parameter(self.W_)
        b = dy.parameter(self.b_)
        context = dy.parameter(self.context_)

        # H is a list of (transduced) vectors
        H = dy.concatenate_cols(H)
        G = W * H
        G = dy.colwise_add(G, b)
        scores = dy.transpose(G) * context
        attn = self.attention(scores)
        weighted_avg = H * attn
        return attn, weighted_avg

    def __repr__(self):
        return "{}(input_dim={}, hidden_dim={})".format(
            self.__class__.__name__,
            self.input_dim,
            self.hidden_dim)


class SparseAttention(SoftAttention):
    def attention(self, x):
        return dy.sparsemax(x)


ATTENTION_TYPES = {
    'soft': SoftAttention,
    'softmax': SoftAttention,
    'sparse': SparseAttention,
    'sparsemax': SparseAttention,
}


def get_attention(model, input_dim, attention_type='soft', hidden_dim=None):

    if not hidden_dim:
        hidden_dim = input_dim

    Attn = ATTENTION_TYPES[attention_type]
    return Attn(model, input_dim, hidden_dim)
