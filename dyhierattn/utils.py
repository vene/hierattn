# Author: Vlad Niculae <vlad@vene.ro>
# Author: Tianze Shi
# License: BSD 3-clause

import dynet as dy


class BaseSubmodel(object):
    def param_collection(self):
        return self.param_collection_

    @classmethod
    def from_spec(cls, spec, model):
        return cls(model, *spec)


class Dense(BaseSubmodel):
    def __init__(self, model, shape, activation):

        self.param_collection_ = model.add_subcollection()
        self.spec = (shape, activation)

        self.activation = activation
        self.shape = shape
        self.W = self.param_collection_.add_parameters(shape)
        self.b = self.param_collection_.add_parameters(shape[0])

    def __call__(self, x):
        b = dy.parameter(self.b)
        W = dy.parameter(self.W)
        out = b + W * x
        if self.activation:
            out = self.activation(out)
        return out


class MultiLayerPerceptron(BaseSubmodel):
    def __init__(self, model, dims, activation):

        self.param_collection_ = model.add_subcollection()
        self.spec = (dims, activation)

        self.layers = []
        self.dropout = 0.0
        last = len(dims) - 2
        for k, (n_in, n_out) in enumerate(zip(dims, dims[1:]), 0):
            self.layers.append(Dense(self.param_collection_,
                                     (n_out, n_in),
                                     activation if k < last else None))

    def __call__(self, x):
        for k, layer in enumerate(self.layers, 1):
            x = layer(x)
            if self.dropout > 0 and k != len(self.layers):
                x = dy.dropout(x, self.dropout)
        return x
