import dynet as dy

from dyhierattn.utils import MultiLayerPerceptron
from dyhierattn.attention import get_attention


class HierarchicalClf(object):

    def __init__(self, vocab_size, n_classes, embed_dim=100, hidden_dim=50,
                 learning_rate=0.1, momentum=0.9, dropout=0.0):
        self.param_collection_ = dy.ParameterCollection()

        self.vocab_size = vocab_size
        self.n_classes = n_classes
        self.embed_dim = embed_dim
        self.hidden_dim = hidden_dim
        self.learning_rate = learning_rate
        self.momentum = momentum
        self.dropout = dropout

        self.embed_ = self.param_collection_.add_lookup_parameters(
            (vocab_size, embed_dim))

        self.word_rnn_ = dy.BiRNNBuilder(num_layers=1,
                                         input_dim=embed_dim,
                                         hidden_dim=hidden_dim * 2,
                                         model=self.param_collection_,
                                         rnn_builder_factory=dy.GRUBuilder)

        self.sent_rnn_ = dy.BiRNNBuilder(num_layers=1,
                                         input_dim=hidden_dim * 2,
                                         hidden_dim=hidden_dim * 2,
                                         model=self.param_collection_,
                                         rnn_builder_factory=dy.GRUBuilder)

        self.word_attn_ = get_attention(attention_type='soft',
                                        input_dim=hidden_dim * 2,
                                        model=self.param_collection_)

        self.sent_attn_ = get_attention(attention_type='soft',
                                        input_dim=hidden_dim * 2,
                                        model=self.param_collection_)

        self.out_ = MultiLayerPerceptron(self.param_collection_,
                                         [hidden_dim * 2, n_classes],
                                         activation=None)

    def build_trainer(self):
        return dy.MomentumSGDTrainer(self.param_collection_,
                                     e0=self.learning_rate,
                                     mom=self.momentum)

    def forward_one(self, doc):
        sents_in = []
        for sent in doc:
            words_emb = [self.embed_[ix] for ix in sent]
            words_enc = self.word_rnn_.transduce(words_emb)
            _, avg = self.word_attn_(words_enc)
            sents_in.append(avg)

        sents_enc = self.sent_rnn_.transduce(sents_in)
        _, avg = self.sent_attn_(sents_enc)

        scores = self.out_(avg)
        return scores

    def set_dropout(self, rate):
        self.sent_rnn_.set_dropout(rate)
        self.word_rnn_.set_dropout(rate)

    def batch_loss(self, X, y):
        dy.renew_cg()
        self.set_dropout(self.dropout)
        scores = (self.forward_one(doc) for doc in X)
        losses = [dy.pickneglogsoftmax(score, y_)
                  for score, y_ in zip(scores, y)]
        avg_loss = dy.average(losses)
        return avg_loss

    def predict(self, X):
        dy.renew_cg()
        self.set_dropout(0.0)
        out = dy.concatenate_cols([self.forward_one(doc) for doc in X])
        out = out.npvalue().argmax(axis=0)
        return out

    def __getstate__(self):
        return {k: v for k, v in self.__dict__.items()
                if not k.endswith('_')}

    def save(self, out):
        import pickle

        self.param_collection_.save(out + ".pc")
        with open(out + '.pkl', 'wb') as f:
            pickle.dump(self, f)
