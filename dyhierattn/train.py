import math
from time import time
from docopt import docopt
from textwrap import dedent

from tqdm import tqdm

import numpy as np
from sklearn.metrics import accuracy_score
from sklearn.utils import check_random_state
from sklearn.externals import joblib

from hierattn.custom_logging import logging
from dyhierattn.model import HierarchicalClf


def batch_slices(n_samples, batch_size=32):
    n_batches = math.ceil(n_samples / batch_size)
    batches = [slice(ix * batch_size, (ix + 1) * batch_size)
               for ix in range(n_batches)]
    return batches


def score(net, X, y, batch_size=32):
    batches = batch_slices(len(y), batch_size)
    y_pred = np.concatenate([net.predict(X[batch])
                             for batch in batches])
    return accuracy_score(y, y_pred)


def train(net, X_train, y_train, X_valid, y_valid, n_epochs=10,
          batch_size=32, report_every=1, save_model='', random_state=None):

    batches = batch_slices(len(y_train), batch_size)
    n_batches = len(batches)

    rng = check_random_state(random_state)

    trainer = net.build_trainer()

    for it in range(n_epochs):

        batch_order = rng.permutation(n_batches)

        tic = time()

        for k in tqdm(batch_order):
            batch = batches[k]
            this_X = X_train[batch]
            this_y = y_train[batch]

            loss = net.batch_loss(this_X, this_y)
            loss.backward()
            trainer.update()

        toc = time()
        if it % report_every == 0:
            valid_acc = score(net, X_valid, y_valid, batch_size=batch_size)
            print('iter {} took {:.2f}s valid_acc {:.2f}'
                  .format(it, toc - tic, valid_acc))

            net.save("{}_acc_{:.2f}_e{:02d}.pt".format(
                save_model,
                valid_acc,
                it))


if __name__ == '__main__':

    usage = dedent("""\
        Train a hierarchical attention model.

        Usage:
            train DATA_FILE [--embed-dim=D --hidden-dim=D --save=F \
        --batch-size=N --epochs=N --learning-rate=N --momentum=N \
        --dynet-mem=N --dynet-gpus=N --dynet-gpu-ids=N --dynet-autobatch=N \
        --dynet-seed=N]

        Options:

        --embed-dim=D      size of embeddings [default: 200]
        --hidden-dim=D     size of hidden layers [default: 50]
        --save=F           file root to save to. [default: ./model]
        --batch-size=N     minibatch size [default: 32]
        --epochs=N         number of epochs [default: 20]
        --dropout=N        dropout ratio [default: 0]
        --learning-rate=N  SG learning rate [default: 0.1]
        --momentum=N       SG momentum [default: 0.9]
    """)

    args = docopt(usage)
    print(args)

    logging.info('loading data')
    data = joblib.load(args['DATA_FILE'])

    vocab_size = data['vocab'].size()
    n_classes = len(data['label_encoder'].classes_)

    #  Model = SlowHierarchicalClf if args['--slow'] else HierarchicalClf
    #  model = Model(vocab_size, n_classes)
    X_train = data['X_train']
    y_train = data['y_train']
    X_valid = data['X_valid']
    y_valid = data['y_valid']

    net = HierarchicalClf(vocab_size, n_classes,
                          embed_dim=int(args['--embed-dim']),
                          hidden_dim=int(args['--hidden-dim']),
                          learning_rate=float(args['--learning-rate']),
                          momentum=float(args['--momentum']),
                          dropout=float(args['--dropout']))

    train(net, X_train, y_train, X_valid, y_valid,
          batch_size=int(args['--batch-size']), n_epochs=int(args['--epochs']),
          save_model=args['--save'], random_state=int(args['--dynet-seed']))
