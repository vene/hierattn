from __future__ import division

import math
import numpy as np

import torch
from torch.autograd import Variable
from onmt import Constants


def batch_slices(n_samples, batch_size=32):
    n_batches = math.ceil(n_samples / batch_size)
    batches = [slice(ix * batch_size, (ix + 1) * batch_size)
               for ix in range(n_batches)]
    return batches


def pad_sentences(doc, cuda=False):
    """Converts a list of indices to a padded torch Variable"""
    n_sents = len(doc)
    len_ix = np.argsort([-len(sent) for sent in doc], kind='mergesort')
    doc = [doc[ix] for ix in len_ix]
    lens = [len(sent) for sent in doc]
    out = doc[0].new(n_sents, lens[0]).fill_(Constants.PAD)
    for i in range(n_sents):
        sent_length = doc[i].size(0)
        out[i].narrow(0, 0, sent_length).copy_(doc[i])

    inv_ix = np.zeros_like(len_ix)
    inv_ix[len_ix] = np.arange(len(len_ix))
    inv_ix = Variable(torch.LongTensor(inv_ix),
                      requires_grad=False).detach()
    out = Variable(out, requires_grad=False)

    if out.is_cuda:
        inv_ix = inv_ix.cuda()

    return out, lens, inv_ix


def pad_docs(docs):
    """Similar to above, but operates directly on variables"""
    n_docs = len(docs)
    _, size = docs[0].size()
    len_ix = np.argsort([-len(doc) for doc in docs], kind='mergesort')
    docs = [docs[ix] for ix in len_ix]
    lens = [len(doc) for doc in docs]

    out = docs[0].data.new(n_docs, lens[0], size).fill_(Constants.PAD)
    out = Variable(out)

    for i in range(n_docs):
        doc_length = docs[i].size(0)
        out[i, :doc_length, :] = docs[i]

    inv_ix = np.zeros_like(len_ix)
    inv_ix[len_ix] = np.arange(len(len_ix))
    inv_ix = Variable(torch.LongTensor(inv_ix),
                      requires_grad=False).detach()

    if out.is_cuda:
        inv_ix = inv_ix.cuda()

    return out, lens, inv_ix
