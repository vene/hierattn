import numpy as np
import torch
from torch import nn
from torch.autograd import Variable
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence

from onmt import Constants

from hierattn.attention import AttentionAverage
from hierattn.utils import pad_sentences, pad_docs


class _BaseHierarchicalClf(nn.Module):

    def __init__(self, vocab_size, n_classes, embed_size=100, hidden_size=50,
                 dropout=0.3):
        self.vocab_size = vocab_size
        self.n_classes = n_classes
        self.embed_size = embed_size
        self.hidden_size = hidden_size
        self.dropout = dropout

        super(_BaseHierarchicalClf, self).__init__()

        self.embed = nn.Embedding(vocab_size, embed_size,
                                  padding_idx=Constants.PAD)

        self.word_encoder = nn.GRU(input_size=embed_size,
                                   hidden_size=hidden_size,
                                   batch_first=True,
                                   bidirectional=True,
                                   dropout=self.dropout)

        self.sent_encoder = nn.GRU(input_size=2 * hidden_size,
                                   hidden_size=hidden_size,
                                   batch_first=True,
                                   bidirectional=True,
                                   dropout=self.dropout)

        self.word_attn = AttentionAverage(2 * hidden_size)
        self.sent_attn = AttentionAverage(2 * hidden_size)

        self.out = nn.Linear(2 * hidden_size, n_classes)


class SlowHierarchicalClf(_BaseHierarchicalClf):

    def forward(self, X_train):
        scores = []
        for doc in X_train:

            sent_reprs = []
            for sent in doc:
                sent = Variable(sent.view(1, -1))
                words_in = self.embed(sent)
                words_out, _ = self.word_encoder(words_in)

                _, word_avg = self.word_attn(words_out)

                sent_reprs.append(word_avg)

            if len(sent_reprs) == 1:
                sent_avg = sent_reprs[0]
            else:
                sent_reprs = torch.cat(sent_reprs, 0).unsqueeze(0)
                _, sent_avg = self.sent_attn(sent_reprs)

            # predict
            out_score = self.out(sent_avg)
            scores.append(out_score)

        scores = torch.cat(scores, 0)
        return(scores)


def _make_detached_long(x, cuda=False):
    y = Variable(torch.LongTensor(x), requires_grad=False).detach()
    if cuda:
        y = y.cuda()
    return y


class HierarchicalClf(_BaseHierarchicalClf):

    def forward(self, X_train):

        # first, collect all sentences in the batch
        all_sents = [sent for doc in X_train for sent in doc]
        lengths = [len(sent) for sent in all_sents]
        offsets = np.cumsum([len(doc) for doc in X_train])
        all_sents_padded, sorted_lens, inv_ix = pad_sentences(all_sents)
        all_sents_embed = self.embed(all_sents_padded)

        all_sents_pack = pack_padded_sequence(all_sents_embed, sorted_lens,
                                              batch_first=True)
        all_sents_enc_pack, _ = self.word_encoder(all_sents_pack)
        all_sents_enc, _ = pad_packed_sequence(all_sents_enc_pack,
                                               batch_first=True)

        # undo sorting
        all_sents_enc = all_sents_enc.index_select(0, inv_ix)

        # test unsorting
        # for sent, sent_enc in zip(all_sents, all_sents_enc):
        #     assert len(sent) == torch.prod(sent_enc != 0, 1).sum()

        # word-level attention
        _, sents_repr = self.word_attn(all_sents_enc, lengths)

        # group sentences by doc
        doc_ixs = np.split(np.arange(len(all_sents)), offsets[:-1])
        doc_ixs = [_make_detached_long(ix, cuda=sents_repr.is_cuda)
                   for ix in doc_ixs]
        sents_by_doc = [sents_repr.index_select(0, ix) for ix in doc_ixs]

        # check correctness
        #  assert len(sents_by_doc) == len(X_train)
        #  for doc, sents_ in zip(X_train, sents_by_doc):
        #      assert(len(doc) == len(sents_))

        # pad, pack and apply sentence-level RNN
        doc_lens = [len(doc) for doc in X_train]
        docs_padded, doc_lens_sorted, doc_inv_ix = pad_docs(sents_by_doc)
        docs_pack = pack_padded_sequence(docs_padded, doc_lens_sorted,
                                         batch_first=True)
        docs_enc_pack, _ = self.sent_encoder(docs_pack)
        docs_enc, _ = pad_packed_sequence(docs_enc_pack, batch_first=True)

        # undo sorting
        docs_enc = docs_enc.index_select(0, doc_inv_ix)

        # sentence-level attention
        _, docs_repr = self.sent_attn(docs_enc, doc_lens)

        out_score = self.out(docs_repr)
        return out_score
