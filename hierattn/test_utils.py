from functools import reduce

import pytest
import numpy as np
from numpy.testing import assert_array_equal

from hierattn.utils import batch_slices


@pytest.mark.parametrize('n_samples', (24, 25, 26))
def test_batch_slices(n_samples):

    idx = np.arange(n_samples)
    batches = batch_slices(n_samples, batch_size=5)

    assert all(len(idx[b]) <= 5 for b in batches)
    assert_array_equal(idx, np.concatenate([idx[batch] for batch in batches]))
    assert len(reduce(np.intersect1d, (idx[batch] for batch in batches))) == 0
