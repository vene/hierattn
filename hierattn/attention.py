import torch
from torch import nn


class AttentionAverage(nn.Module):
    def __init__(self, hidden_size):
        self.hidden_size = hidden_size
        super(AttentionAverage, self).__init__()

        self.linear = nn.Linear(hidden_size, hidden_size)
        self.context = nn.Linear(hidden_size, 1, bias=False)
        self.sm = nn.Softmax()
        self.tanh = nn.Tanh()

    def forward(self, H, lengths=None):
        """
        H: context, shape=(batch_sz, source_length, dim)
        """

        batch_size, seq_len, sz = H.size()
        assert sz == self.hidden_size

        # compute alignment score with context vector, independently over seqs
        U = self.linear(H.view(-1, self.hidden_size))
        U = self.tanh(U)
        scores = self.context(U)

        # normalize scores for each sequence
        scores = scores.view(batch_size, seq_len)

        # softmax-specific dealing with masks: set to -inf
        if lengths:
            for i in range(batch_size):
                if lengths[i] < seq_len:
                    scores[i, lengths[i]:].data.fill_(-float('inf'))
        attn = self.sm(scores)

        # take average of input sequences, weighted by attention
        weighted_avg = torch.bmm(H.transpose(1, 2), attn.unsqueeze(2))
        weighted_avg = weighted_avg.squeeze(2)

        return attn, weighted_avg
