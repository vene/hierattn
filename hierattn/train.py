from docopt import docopt
from textwrap import dedent
from time import time

from tqdm import tqdm

import torch
from torch import nn
from torch.autograd import Variable

from hierattn.utils import batch_slices
from hierattn.model import HierarchicalClf, SlowHierarchicalClf


def score(model, X, y, batch_size=32, cuda=False):
    model.eval()
    batches = batch_slices(len(y), batch_size)
    y_pred = []
    for batch in batches:
        this_X = X[batch]

        if cuda:
            this_X = [[sent.cuda() for sent in doc] for doc in this_X]

        scores = model(this_X)

        if cuda:
            # return to CPU for final evaluation
            scores = scores.cpu()

        _, this_y_pred = torch.max(scores.data, dim=1)
        y_pred.append(this_y_pred)

    y_pred = torch.cat(y_pred)

    train_acc = y_pred.squeeze().eq(y).float().mean() * 100
    return train_acc


def train(model, X, y, X_valid, y_valid, n_epochs=10, batch_size=32,
          learning_rate=0.1, momentum=0.9, report_every=1, save_model="",
          optimizer='sgd', clip=5, start_epoch=0, optimizer_state=None, cuda=False):

    loss_func = nn.CrossEntropyLoss()

    if cuda:
        model.cuda()
        loss_func.cuda()

    if optimizer == 'sgd':
        optim = torch.optim.SGD(model.parameters(), lr=learning_rate,
                                momentum=momentum)
    elif optimizer == 'adam':
        optim = torch.optim.Adam(model.parameters(), lr=learning_rate)

    elif optimizer == 'rmsprop':
        optim = torch.optim.RMSProp(model.parameters(), lr=learning_rate,
                momentum=momentum)

    else:
        raise ValueError('unexpected optimizer')

    if optimizer_state is not None:
        optim.load_state_dict(optimizer_state)

    print(model)
    print(optim)

    batches = batch_slices(len(y), batch_size)
    n_batches = len(batches)

    for it in range(start_epoch, start_epoch + n_epochs):

        tic = time()

        batch_order = torch.randperm(n_batches)
        for k in tqdm(range(n_batches)):

            # select batch
            k = batch_order[k]
            batch = batches[k]
            this_X = X[batch]
            this_y = Variable(y[batch], requires_grad=False)

            if cuda:
                this_X = [[sent.cuda() for sent in doc] for doc in this_X]
                this_y = this_y.cuda()

            model.train()
            optim.zero_grad()

            scores = model(this_X)
            obj = loss_func(scores, this_y)
            obj.backward()

            if clip:
                nn.utils.clip_grad_norm(model.parameters(), clip)

            optim.step()

        toc = time()

        if it % report_every == 0:
            valid_acc = score(model, X_valid, y_valid, batch_size=batch_size,
                              cuda=cuda)
            print(('iter {} took {:.2f}s valid_acc {:.2f}').format(
                it, toc - tic, valid_acc))

            # save snapshot
            checkpoint = {
                'model': model.state_dict(),
                'epoch': it,
                'optim': optim
            }
            torch.save(checkpoint, "{}_acc_{:.3f}_e{:04d}.pt".format(
                save_model,
                valid_acc,
                it))


if __name__ == '__main__':

    usage = dedent("""\
        Train a hierarchical attention model.

        Usage:
            train DATA_FILE [--slow --rng-seed=N --gpu=ID... --save=F \
        --start-from=F --learning-rate=N --momentum=N --embed-dim=D \
        --hidden-dim=D --dropout=N --optimizer=T --clip=N --batch-size=N \
        --epochs=N]

        Options:

        --slow             whether to use slow reference implementation
        --rng-seed=N       random seed. [default: 0]
        --gpu=ID...        gpu ids to use
        --save=F           file root to save to. [default: ./model]
        --start-from=F     file to continue from
        --embed-dim=D      size of embeddings [default: 200]
        --hidden-dim=D     size of hidden layers [default: 50]
        --batch-size=N     minibatch size [default: 32]
        --epochs=N         number of epochs [default: 20]
        --learning-rate=N  SG learning rate [default: 0.1]
        --optimizer=T      (sgd|adam|rmsprop) [default: sgd]
        --momentum=N       SG momentum [default: 0.9]
        --clip=N           gradient clipping [default: 0]
        --dropout=N        dropout ratio [default: 0.3]
    """)

    args = docopt(usage)

    gpu_ids = [int(i) for i in args['--gpu']]
    seed = int(args['--rng-seed'])
    torch.manual_seed(seed)

    if gpu_ids:
        torch.cuda.manual_seed(seed)
        torch.cuda.set_device(gpu_ids[0])
        cuda = True
    else:
        cuda = False

    if len(gpu_ids) > 1:
        raise NotImplemented('multigpu not implemented yet')

    data = torch.load(args['DATA_FILE'])

    vocab_size = data['vocab'].size()
    n_classes = len(data['label_encoder'].classes_)

    Model = SlowHierarchicalClf if args['--slow'] else HierarchicalClf
    model = Model(vocab_size, n_classes, embed_size=int(args['--embed-dim']),
                  hidden_size=int(args['--hidden-dim']),
                  dropout=float(args['--dropout']))

    start_epoch = 0
    optim_state = None
    if args['--start-from']:
        checkpoint = torch.load(args['--start-from'])
        model.load_state_dict(checkpoint['model'])
        start_epoch = checkpoint['epoch'] + 1
        optim_state = checkpoint['optim'].state_dict()

    X_train = data['X_train']
    y_train = data['y_train']
    X_valid = data['X_valid']
    y_valid = data['y_valid']
    y_train = torch.from_numpy(y_train.copy())
    y_valid = torch.from_numpy(y_valid.copy())

    train(model,
          X_train,
          y_train,
          X_valid,
          y_valid,
          batch_size=int(args['--batch-size']),
          n_epochs=int(args['--epochs']),
          optimizer=args['--optimizer'],
          optimizer_state=optim_state,
          learning_rate=float(args['--learning-rate']),
          momentum=float(args['--momentum']),
          save_model=args['--save'],
          clip=float(args['--clip']),
          start_epoch=start_epoch,
          cuda=cuda)
