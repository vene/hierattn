from textwrap import dedent
from docopt import docopt

import numpy as np

from sklearn.preprocessing import LabelEncoder
from sklearn.utils import shuffle
from onmt import Dict, Constants

from hierattn.custom_logging import logging


def _read_text_ss(filename):

    X = []
    y = []

    with open(filename, encoding='utf8') as f:
        for line in f:
            _, _, stars, text = line.split("\t\t")
            stars = stars.strip()
            sents = [sent.strip().split()
                     for sent in text.strip().split('<sssss>')]

            y.append(stars)
            X.append(sents)

    return X, y


def read_dataset_ss(train, valid, vocab_size=10000):
    X_train, y_train = _read_text_ss(train)
    X_valid, y_valid = _read_text_ss(valid)

    lbl_enc = LabelEncoder()
    y_train_enc = lbl_enc.fit_transform(y_train)
    y_valid_enc = lbl_enc.transform(y_valid)

    vocab = Dict([Constants.PAD_WORD, Constants.UNK_WORD,
                  Constants.BOS_WORD, Constants.EOS_WORD])

    for doc in X_train:
        for sent in doc:
            for w in sent:
                vocab.add(w)

    full_sz = vocab.size()
    vocab = vocab.prune(vocab_size)
    logging.info("Created dictionary of size {} (pruned from {})".format(
        vocab.size(), full_sz))

    def vectorize_docs(X):
        return [[vocab.convertToIdx(sent, Constants.UNK_WORD) for sent in doc]
                for doc in X]

    X_train_vect = vectorize_docs(X_train)
    X_valid_vect = vectorize_docs(X_valid)

    return X_train_vect, y_train_enc, X_valid_vect, y_valid_enc, lbl_enc, vocab


if __name__ == '__main__':

    usage = dedent("""\
        Preprocess the data.

        Usage:
            preprocess TRAIN_FILE VALID_FILE OUT_FILE [--no-shuffle --no-sort \
        --vocab-size=N --rng-seed=N]

        Options:
            --vocab-size=N  maximum number of words. [default: 10000]
            --rng-seed=N    seed used in shuffling. [default: 0]
    """)

    args = docopt(usage)

    X_train, y_train, X_valid, y_valid, lbl_enc, vocab = read_dataset_ss(
        train=args['TRAIN_FILE'], valid=args['VALID_FILE'],
        vocab_size=int(args['--vocab-size']))

    # shuffle
    if not args['--no-shuffle']:
        X_train, y_train = shuffle(X_train, y_train,
                                   random_state=int(args['--rng-seed']))

    # sort by sentence length
    if not args['--no-sort']:
        sents_per_doc = [len(doc) for doc in X_train]
        ix = np.argsort(sents_per_doc, kind='mergesort')  # stable
        X_train = np.take(X_train, ix, mode='clip').tolist()
        y_train = np.take(y_train, ix, mode='clip')

    save_data = {
        'vocab': vocab,
        'label_encoder': lbl_enc,
        'X_train': X_train,
        'y_train': y_train,
        'X_valid': X_valid,
        'y_valid': y_valid
    }

    out_fn = args['OUT_FILE']

    if out_fn.endswith('.pt'):
        import torch
        torch.save(save_data, out_fn)
    elif out_fn.endswith('.jbl'):
        from sklearn.externals import joblib
        joblib.dump(save_data, out_fn)

    logging.info('Saved to {}'.format(args['OUT_FILE']))
